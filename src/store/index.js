import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  byteUsageTotal: 0,
  byteUsagePerDomain: [],
  skip: false,
  jwt: ''
}

const getters = {
  byteUsageTotal: state => state.byteUsageTotal,
  byteUsagePerDomain: state => state.byteUsagePerDomain,
  byteUsagePerDomainChart: state => state.byteUsagePerDomain.map(({ domain, bytes, gco2 }) => ({ label: domain, value: bytes, sValue: gco2 })),
  logged: state => state.jwt !== '' || state.skip,
  jwt: state => state.jwt,
  claim: state => state.jwt.split('.')[1]
}

const actions = {
  updateStats ({ commit }, stats) {
    // Total
    const usageTotal = Object.values(stats).map(({ bytes}) => bytes).reduce((a, b) => a + b)
    // Convert Object to Array of Object
    const statsFormatted = Object.entries(stats).map(([key, value]) => {
      return { domain: key, bytes: value.bytes }
    })
    commit('SET_USAGE_TOTAL', usageTotal)
    commit('SET_USAGE_PER_DOMAIN', statsFormatted)
  },
  login ({ commit }, jwt) {
    commit('SET_JWT', jwt)
  },
  skip ({ commit }) {
    commit('SET_SKIP', true)
  },
  unskip ({ commit }) {
    commit('SET_SKIP', false)
  },
  unsetjwt ({ commit }) {
    commit('SET_JWT', '')
  }
}

const mutations = {
  SET_USAGE_TOTAL (state, usage) {
    state.byteUsageTotal = usage
  },
  SET_USAGE_PER_DOMAIN (state, usage) {
    state.byteUsagePerDomain = usage
  },
  SET_LOGGED (state, status) {
    state.logged = status
  },
  SET_SKIP (state, status) {
    state.skip = status
  },
  SET_JWT (state, payload) {
    state.jwt = payload
  }
}

export default new Vuex.Store({
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
})
