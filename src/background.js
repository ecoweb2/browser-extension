global.browser = require('webextension-polyfill')
const axios = require('axios')
const equiv = require('./equiv')
const urlConso = 'https://ecoweb-efrei-production.herokuapp.com/api/consumptions'
let numberOfRequest = 0

function getJwt () {
  return localStorage.getItem('jwt')
}

function deleteJwt () {
  localStorage.removeItem('jwt')
}

function getLastSent () {
  return localStorage.getItem('last_sent') || 0
}

function setLastSent (value) {
  return localStorage.setItem('last_sent', value)
}

function consoDiff (current) {
  return current - getLastSent()
}

function totalGCO2 () {
  return browser.storage.local.get()
    .then(result => {
      const consoDomains = Object.values(result).map(({ gco2 }) => gco2).reduce((a, b) => a + b)
      const consoDevice = equiv.getDeviceC02(localStorage.getItem('duration') || 0)
      return consoDevice + consoDomains
    })
}

function sendUserConsumption () {
  totalGCO2()
    .then(conso => axios.post(urlConso, { conso: consoDiff(conso) }, { headers: { Authorization: 'Bearer ' + getJwt(), ContentType: 'application/json' } })
      .then(result => (result.status === 201) ? setLastSent(conso) : null)
      .catch(err => (err.status === 401) ? deleteJwt() : null)
    )
}

function checkIter () {
  numberOfRequest += 1
  if (!getJwt()) return
  if (numberOfRequest % 50 === 0) sendUserConsumption()
}

function getContentLength (response) {
  const content = response.responseHeaders.find(header => header.name.toLowerCase() === 'content-length')
  return content ? Number.parseInt(content.value, 10) : 0
}

function parseDomain (url) {
  if (url.includes('chrome')) return 'chrome'
  return url.match('(?:http:\\/\\/\\.www|https:\\/\\/www\\.|http:\\/\\/|www\\.|https:\\/\\/)([^\\/]+)')[1]
}

function genKV (domain, usage) {
  const obj = {}
  obj[domain] = { gco2: equiv.bytesToCo2(usage), bytes: usage }
  return obj
}

function save (response) {
  // Generate KV to be stored : {domain=usage}c
  checkIter()
  const byteSize = getContentLength(response)
  if (byteSize === 0) return
  const url = response.initiator || response.originUrl
  if (url === undefined) return
  const domain = parseDomain(url)
  const kv = genKV(domain, byteSize)

  // Query the storage to check if the value should be initialized or updated
  browser.storage.local.get().then(stored => {
    if (stored[domain] === undefined) {
      console.log('NEW ' + domain )
      browser.storage.local.set(kv).catch(err => {
        throw err
      })
    } else {
      console.log('UPDATE ' + domain)
      const updatedBytes = kv[domain].bytes + stored[domain].bytes
      kv[domain] = { gco2: equiv.bytesToCo2(updatedBytes), bytes: updatedBytes }
      browser.storage.local.set(kv).catch(err => {
        throw err
      })
    }
  })
}

function updateDuration () {
  const duration = Number.parseInt(localStorage.getItem('duration'),10) || 0
  localStorage.setItem('duration', duration+30)
}

browser.webRequest.onHeadersReceived.addListener(
  data => save(data),
  { urls: ['<all_urls>'] },
  ['responseHeaders']
)

setInterval(updateDuration, 30000)
