
const defaultCarbonIntensityFactorIngCO2PerKWh = 519
const kWhPerByteDataCenter = 0.000000000072
const kWhPerByteNetwork = 0.000000000152
const kWhPerMinuteDevice = 0.00021

 const carbonIntensityFactorIngCO2PerKWh = {
   regionEuropeanUnion: 276,
   regionFrance: 34.8,
   regionUnitedStates: 493,
   regionChina: 681,
   regionOther: defaultCarbonIntensityFactorIngCO2PerKWh
 }

export function bytesToCo2 (bytes) {
  const kWhDataCenterTotal = bytes * kWhPerByteDataCenter
  const GESDataCenterTotal = kWhDataCenterTotal * defaultCarbonIntensityFactorIngCO2PerKWh
  const kWhNetworkTotal = bytes * kWhPerByteNetwork
  const GESNetworkTotal = kWhNetworkTotal * defaultCarbonIntensityFactorIngCO2PerKWh
  // const kWhDeviceTotal = duration * kWhPerMinuteDevice
  // const GESDeviceTotal = kWhDeviceTotal * carbonIntensityFactorIngCO2PerKWh[userLocation]
  // const kWhTotal = Math.round(1000 * (kWhDataCenterTotal + kWhNetworkTotal)) / 1000
  return GESDataCenterTotal + GESNetworkTotal
}

export function getDevicekWh(duration){
  return duration * (kWhPerMinuteDevice/60)
}

export function getDeviceC02(duration){
  return getDevicekWh(duration) * carbonIntensityFactorIngCO2PerKWh.regionFrance
}